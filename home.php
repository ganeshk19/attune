<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tag Survey @ CSE - 02</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="css/style.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="jquery/jquery-1.11.1.min.js"></script>
    <script src="bootstrap/3.3.0.bootstrap.min.js"></script>
    <link href="bootstrap/3.3.0.bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery.min.js"></script>
    <?php
		
		session_start();
		
		$dbServername = "localhost";
		$dbUsername = "root";
		$dbPass = "";
		$dbName = "tag_survey";
		$conn = mysqli_connect($dbServername , $dbUsername, $dbPass, $dbName);
		$sql = "SELECT * FROM users";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_assoc($result);
		
	?>
</head>

<body id="Home_page">

<div class="container">
    <nav class="navbar navbar-light" style="background-color: #f7f9fc;">
      <div class="container">
        <div class="navbar-header">
          <span class="navbar-text">CSE - 02</span>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><span class="navbar-text">Welcome <?php echo $row['id_no']; ?></span></li>
            <li>
              <a class="btn btn-primary" href="logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    
  <form method="POST"">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="tag1">#Tag_01</label>
      <input type="number" class="form-control" name="tag1" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">

      <label for="tag2">#Tag_02</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag3">#Tag_03</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="tag4">#Tag_04</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag5">#Tag_05</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag6">#Tag_06</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="tag7">#Tag_07</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag8">#Tag_08</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag9">#Tag_09</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="tag10">#Tag_10</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag11">#Tag_11</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag12">#Tag_12</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="tag13">#Tag_13</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag14">#Tag_14</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
    <div class="form-group col-md-4">
      <label for="tag15">#Tag_15</label>
      <input type="number" class="form-control" name="tag" placeholder="Enter Roll Number" required min="1" max="65">
    </div>
  </div>
  <div class="form-row">
	<div class="form-group col-md-4 offset-md-4"></div>
	<div class="form-group col-md-4 offset-md-4">
		<center><button type="submit" class="btn btn-primary" >Submit</button></center>
	</div>
  </div>
  
</form>
       
</div>

</body>
</html>


