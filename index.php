<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login @ CSE-02</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="jquery/jquery-1.11.1.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
    <script src="jquery/jquery.min.js"></script>
    
    <?php
		
		session_start();
		
		if(isset($_POST['login'])) {
			
			$dbServername = "localhost";
			$dbUsername = "root";
			$dbPass = "";
			$dbName = "tag_survey";
	
			$conn = mysqli_connect($dbServername , $dbUsername, $dbPass, $dbName);
			
			$id_no = mysql_real_escape_string($_POST['id_no']);
			$passwd = mysql_real_escape_string($_POST['password']);
			
			if(empty($id_no) || empty($passwd)) {
				header("Location: index.php?login=empty");
				exit();
			} else {
				$sql = "SELECT * FROM users WHERE id_no = '$id_no' and password = '$passwd'";
				$result = mysqli_query($conn, $sql);
				$resultCheck = mysqli_num_rows($result);
				if($resultCheck < 1) {
					header("Location: index.php?login=error1");
					exit();
				} else {
					if ($row = mysqli_fetch_assoc($result)) {
							$_SESSION['id_no'] = $row['id_no'];
							header("Location: home.php");
							exit();
					}
				}
			}
		}
		
	?>
    
</head>

<body id="LoginForm">
<div class="container">
	<div class="login-form">
		<div class="main-div">
			<div class="icon-fafa">
				<i class="fa fa-user fa-4x" ></i>
			</div>
			<div class="panel">
				<p>Welcome Guest @ CSE-02</p>
			</div>
			<form id="Login" method="POST">

				<div class="form-group">
					<input type="text" class="form-control" name="id_no" placeholder="ID Number">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="Password">
				</div>
				<button type="submit" name="login" class="btn btn-primary">Login</button>
			</form>
		</div>
	</div>
</div>


</body>
</html>


